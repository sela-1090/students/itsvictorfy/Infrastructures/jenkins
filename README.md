# Jenkins
## Install
- helm repo add jenkins https://charts.jenkins.io
- helm repo update
- helm upgrade --install jenkins jenkins/jenkins -f values.yaml --namespace cicd

# Jenkins Pipeline for Docker Image and Helm Chart Deployment

This Jenkins pipeline automates the process of building a Docker image, creating a Helm chart, and pushing both the Docker image and Helm chart to the respective repositories. The pipeline is designed to work in a Kubernetes environment using Jenkins agents.

## Prerequisites

1. Jenkins with Kubernetes plugin configured.
2. Kubernetes cluster with a 'slave' label.
3. Necessary credentials set up in Jenkins for Docker Hub, GitHub, and Kubernetes.
4. 'build-pod.yaml' file defining the build pod configuration.

## Pipeline Overview

1. **Build Docker Image:**
   - Build a Docker image using the provided Dockerfile.
   - Update the Helm chart values with the Docker image tag and namespace.

2. **Build Helm Chart:**
   - Package the Helm chart with the updated values.
   
3. **Docker & Helm Login and Push:**
   - Log in to Docker Hub using provided credentials.
   - Push the built Docker image to Docker Hub.
   - Push the Helm chart to a specified OCI registry.

4. **Update Helm Values:**
   - Depending on the branch (main, stage, dev):
     - Clone the 'finalproj-argo' repository.
     - Update the Helm chart values with the Docker image tag and namespace based on the branch.
     - Commit and push the changes to the respective branch.

